my_path := $(call my-dir)
VENDOR_PATH := vendor/android-generic
LOCAL_PATH := $(my_path)
include $(CLEAR_VARS)

# Inherit common Bliss OS stuff
$(call inherit-product, vendor/android-generic/addon.mk)

# Inherit comon android-generic
$(call inherit-product,vendor/android-generic/config/common.mk)

# Inherit vendors
$(call inherit-product,vendor/android-generic/config/vendors.mk)

# Include Bliss x86 overlays
PRODUCT_PACKAGE_OVERLAYS += vendor/android-generic/overlay/x86

# ROM specific overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += $(VENDOR_PATH)/patches/google_diff/pc_vendor_patches/$(CURRENT_ROM)/overlay
DEVICE_PACKAGE_OVERLAYS += $(VENDOR_PATH)/patches/google_diff/pc_vendor_patches/$(CURRENT_ROM)/overlay/common
#~ $(echo "android_pc.mk CURRENT_ROM=$(CURRENT_ROM)")

# Boot animation
TARGET_SCREEN_HEIGHT := 1080
TARGET_SCREEN_WIDTH := 1080
TARGET_BOOTANIMATION_HALF_RES := false

# If using gms
ifeq ($(USE_GMS),true)
$(call inherit-product, vendor/gms/config.mk)
endif

# If using fdroid
ifeq ($(USE_FDROID),true)
$(call inherit-product-if-exists, vendor/foss/foss.mk)
# Get GMS
$(call inherit-product-if-exists,vendor/microg/microg.mk)
# FOSS apps
PRODUCT_PACKAGES += \
	FDroid \
	FDroidPrivilegedExtension \
	FakeStore \
	Phonesky \
	DroidGuard \
	GmsCore \
	privapp-permissions-com.google.android.gms.xml \
	GsfProxy \
	MozillaNlpBackend \
	NominatimNlpBackend \
	com.google.android.maps \
	com.google.android.maps.jar \
	com.google.android.maps.xml \
	OpenWeatherMapWeatherProvider \
	additional_repos.xml

endif

# If using fdroid
ifeq ($(USE_FOSS),true)
$(call inherit-product-if-exists, vendor/foss/foss.mk)

# FOSS apps
PRODUCT_PACKAGES += \
	org.schabi.newpipe \
	org.smssecure.smssecure \
	net.osmand.plus \
	org.mozilla.fennec_fdroid \
	ws.xsoh.etar \
	de.grobox.liberario \
	com.artifex.mupdf.viewer.app \
	com.aurora.store \
	com.fsck.k9 \
	com.etesync.syncadapter \
	com.nextcloud.client \
	org.tasks \
	org.mariotaku.twidere \
	com.pitchedapps.frost \
	com.keylesspalace.tusky \
	co.pxhouse.sas \
	com.simplemobiletools.gallery.pro \
	com.aurora.adroid \
	FDroidPrivilegedExtension \
	Provision

endif

PRODUCT_PROPERTY_OVERRIDES += \
    ro.mot.deep.sleep.supported=true 
    
# Required packages
PRODUCT_PACKAGES += \
    LatinIME

PRODUCT_SHIPPING_API_LEVEL := 19

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.activities_on_secondary_displays.xml:system/etc/permissions/android.software.activities_on_secondary_displays.xml \
    frameworks/native/data/etc/android.software.midi.xml:system/etc/permissions/android.software.midi.xml \
    frameworks/native/data/etc/android.software.picture_in_picture.xml:system/etc/permissions/android.software.picture_in_picture.xml \
    frameworks/native/data/etc/android.software.print.xml:system/etc/permissions/android.software.print.xml \
    frameworks/native/data/etc/android.software.webview.xml:system/etc/permissions/android.software.webview.xml \
    frameworks/native/data/etc/android.hardware.gamepad.xml:system/etc/permissions/android.hardware.gamepad.xml \

# Enable MultiWindow
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.debug.multi_window=true
    persist.sys.debug.desktop_mode=true

# Optional packages
PRODUCT_PACKAGES += \
    LiveWallpapersPicker \
    PhotoTable \
    Terminal

# Custom Lineage packages
PRODUCT_PACKAGES += \
    htop \
    nano 

# Common Packages
PRODUCT_PACKAGES += \
    ThemePicker \
    SoundPickerPrebuilt

# Set Bliss Desktop Mode by default
# Use 'export BLISS_DESKTOPMODE=true' or set 
# 'BLISS_DESKTOPMODE := true' within BoardConfig.mk. 
ifeq ($(BLISS_DESKTOPMODE),true)

# Remove packages
GAPPS_EXCLUDED_PACKAGES += \
    Eleven \
    Jelly \
    Launcher3 \

endif

# We add in gBoard now, so make it override latinIME
GAPPS_EXCLUDED_PACKAGES += \
    LatinIME \
    
# Exchange support
PRODUCT_PACKAGES += \
    Exchange2 \

# Boot Animation
# PRODUCT_PACKAGES += \
#     bootanimation.zip



# Legacy houdini files
define addon-copy-from-system
$(shell python "vendor/android-generic/copy_files.py" "vendor/android-generic/$(1)/" "$(2)" "$(PLATFORM_SDK_VERSION)")
endef

define addon-copy-to-system
$(shell python "vendor/android-generic/copy_files.py" "vendor/google/chromeos-x86/proprietary/$(1)/" "$(2)" "$(PLATFORM_SDK_VERSION)")
endef

# Houdini addons

ifeq ($(USE_PRIV_HOUDINI),true)

$(call inherit-product, vendor/bliss_priv/device-vendor.mk)

endif


ifeq ($(USE_HOUDINI),true)


# !!EXPERIMENTAL!!
# QEMU-based native bridge for Android-x86 - https://github.com/goffioul/ax86-nb-qemu
ifeq ($(USE_X86LIBNB),true)

# Remove packages
PRODUCT_PACKAGES += \
	libqemu-core \
	libqemu-target \
	qemu-arm \
	libqemu-capstone \
	libnb-qemu \
	libnb-qemu-EGL \
    libnb-qemu-guest
else
# Get proprietary files if any exists
$(call inherit-product, vendor/google/chromeos-x86/target/houdini.mk)
$(call inherit-product, vendor/google/chromeos-x86/target/native_bridge_arm_on_x86.mk)

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += persist.sys.nativebridge=1 
	
# PRODUCT_SYSTEM_DEFAULT_PROPERTIES += ro.enable.native.bridge.exec=1
	
# Copy files
#PRODUCT_COPY_FILES += $(call addon-copy-from-system,system,bin)
#PRODUCT_COPY_FILES += $(call addon-copy-from-system,system,lib)
PRODUCT_COPY_FILES += $(call addon-copy-to-system,houdini,bin)
PRODUCT_COPY_FILES += $(call addon-copy-to-system,houdini,etc)
PRODUCT_COPY_FILES += $(call addon-copy-to-system,houdini,lib)

endif

endif

# Widevine addons
ifeq ($(USE_WIDEVINE),true)
# Copy files
PRODUCT_COPY_FILES += $(call addon-copy-to-system,widevine,vendor) 

endif

#PRODUCT_COPY_FILES += \
#    vendor/bliss_os/bootanimation/bootanimation.zip:system/media/bootanimation.zip

PRODUCT_COPY_FILES += \
	x86_installer/bin/Androidx86-Installv28.5800.exe:Androidx86-Installv28.5800.exe
	
